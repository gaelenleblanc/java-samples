
package cs345leblang.interpret;

import java.util.*;
/**
 *
 * @author Gaelen LeBlanc (gaelen.leblanc@gmail.com)
 */
public class SelectMessage implements Message{
    
    private ArrayList<Message> messages = new ArrayList<>();
    
    public SelectMessage(Message... msgs){
        messages.addAll(Arrays.asList(msgs));
    }
    
    @Override
    public void print(GamePrintStream out){
        messages.get(0).print(out);
    }


    @Override
    public void printArgs(GamePrintStream out, String... args){
        throw new UnsupportedOperationException("Method Not Supported For Select Message.");
    }

    @Override
    public void printAlt(GamePrintStream out, int alt){
        if(alt>=messages.size()){
            messages.get(messages.size()-1).print(out);
        }
        else if(alt<0){
            messages.get(0).print(out);            
        }
        else{
            messages.get(alt).print(out);
        }
    }
}
