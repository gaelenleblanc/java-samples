

package cs345leblang.interpret;
import java.util.*;
/**
 *
 * @author Gaelen LeBlanc
 */
public class CycleMessage implements Message{
    
    private ArrayList<Message> messages = new ArrayList<>();
    private int i;
    
    public CycleMessage (Message... msgs){
        messages.addAll(Arrays.asList(msgs));
        i = 0;
    }
    
    @Override
    public void print(GamePrintStream out){
        messages.get(i).print(out);
        i = (i+1) % messages.size();
    }
    
    @Override
    public void printArgs(GamePrintStream out, String... args){
        messages.get(i).printArgs(out, args);
        i = (i+1) % messages.size();
    } 
    
    @Override
    public void printAlt (GamePrintStream out, int alt){
        messages.get(alt).printAlt(out, alt);
    }
}
