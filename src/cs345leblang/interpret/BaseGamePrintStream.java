

package cs345leblang.interpret;

import java.io.*;

/**
 * This class is a GamePrintStream.
 * 
 * @author Gaelen LeBlanc (gaelen.leblanc@gmail.com)
 */
public class BaseGamePrintStream implements GamePrintStream {
	
	public final int MAX_LINE;
	public final String LINE_SEP = "\n";
	
	private PrintStream out;
	private StringBuilder buf = new StringBuilder();
	
	public BaseGamePrintStream(PrintStream out, int maxLine) {
		this.out = out;
		this.MAX_LINE = maxLine;
	}

	@Override
	public void print(String s) {
		buf.append(s);
		doOutput(false);
	}
	
	@Override
	public void println(String s) {
		buf.append(s);
		println();
	}
	
	@Override
	public void println() {
	    buf.append(LINE_SEP);
	    doOutput(false);
	}
	
	@Override
	public void printf(String s, Object... objs) {
		buf.append(String.format(s, objs));
		doOutput(false);
	}
	
	@Override
	public void flush() {
	    doOutput(true);
	}

    private void doOutput(boolean flush) {

        
        //output contents of buffer
        out.print(buf.toString().replace("\n", System.lineSeparator()));
        buf.setLength(0);//clear buffer
    }
}
