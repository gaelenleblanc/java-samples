

package cs345leblang.interpret;

import cs345leblang.game.Game;

/**
 * This is the interface defines the method for an Event Handler.
 * 
 * @author Gaelen LeBlanc (gaelen.leblanc@gmail.com)
 */
public interface HandlerMethod {

	public void doHandler(Game game, Event e);
	
}
