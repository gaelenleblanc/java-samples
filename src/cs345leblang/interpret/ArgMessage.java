

package cs345leblang.interpret;

/**
 *
 * @author Gaelen LeBlanc (gaelen.leblanc@gmail.com)
 */
public class ArgMessage implements Message{
    private int index;
    
    public ArgMessage(int index){
        this.index=index;
    }
    
    @Override
    public void print (GamePrintStream out){
        throw new UnsupportedOperationException("Method not supported for ArgMessage.");
    }
    
    @Override
    public void printArgs(GamePrintStream out, String... args){
        out.print(args[index]);
    }
    
    @Override
    public void printAlt(GamePrintStream out, int alt){
        throw new UnsupportedOperationException("Method not supported for ArgMessage.");
    }
    
}
