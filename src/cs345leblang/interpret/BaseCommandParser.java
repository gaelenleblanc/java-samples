

package cs345leblang.interpret;

import java.util.*;
import java.io.*;
import cs345leblang.game.*;

/**
 * This class is the command parser for a game. 
 * @author Gaelen LeBlanc (gaelen.leblanc@gmail.com)
 */
public class BaseCommandParser implements CommandParser {

	private Game game;
        
        private ArrayList<Action> allActions = new ArrayList<>();
	private ArrayList<Terms> allTerms = new ArrayList<>();
	private ArrayList<Word> allWords = new ArrayList<>();
	private ArrayList<Word> listOfPrefixWords = new ArrayList<>();
	private ArrayList<Event> eventQueue = new ArrayList<>();
        private ArrayList<Handler> allHandlers = new ArrayList<>();


	public BaseCommandParser(Game game) {
	    this.game = game;
            game.parser = this;
	}
	
    
        @Override
	public void run() {
            allActions = game.getallActions();
            allTerms = game.getAllTerms();
            allWords = game.getAllWords();
            listOfPrefixWords = game.getPrefixWords();
            allHandlers = game.getAllHandlers();
            queueEvent(Event.INIT);
	    while (!game.exit) {
                runEvents();
                if(game.exit){
                    break;
                }
                runOneCommand();
		}
		
	}
	
        public void runEvents(){
          

            while(!eventQueue.isEmpty()){
                
                Event e = eventQueue.remove(0);
                
                for(Handler h: allHandlers){
                    
                    if(h.getEvent() == e){
                        h.getHandlerMethod().doHandler(game, e);
                    }
                }
            }
        }
    
	@Override
	public boolean setExit(boolean exit) {
		boolean old = game.exit;
		game.exit = exit;
		return old;
	}

        private void runOneCommand() {

		String input;
		Scanner in = new Scanner(System.in);
                System.out.println();
		System.out.println("? ");
                input = in.nextLine();
                input = input.trim();
		ArrayList<String> inputList = new ArrayList<>(Arrays.asList(input.split("\\s")));
		
		Word toCheck1 = null;
		Word toCheck2 = null;
		
                
                
		
		for(int i = inputList.size()-1; i>=0; i--){
                    if(Game.noiseWords.termContainsString(inputList.get(i)) || Game.noiseWords.termHasPrefixString(inputList.get(i) ) ){
			
			inputList.remove(i);
                    }
		}

                
		if(inputList.isEmpty()){
			System.out.println("Please Input a Valid Command.");
		}
		else if(inputList.size() >= 3){
			System.out.println("Error: Too many arguments.");
		}
                
		else{
                    
                    //procedure for one term command
                    if(inputList.size()==1){
                        ArrayList<Word> temp = getAllWordsBeginningWith(inputList.get(0));
                       
                        if(temp.isEmpty()){
                            //there is no match
                            System.out.println("Error: Unable to Find Exact or Prefix Match for Given Command.");
                        }
                        else{
                            //check for exact match
                            for(int i = 0; i<temp.size(); i++){
                                if(temp.get(i).getWord().equalsIgnoreCase(inputList.get(0))){
                                    toCheck1 = temp.get(i);
                                    
                                }
                            }
                            
                        }
                        //toCheck1 is null if an exact match was not found   
                        if(toCheck1 == null){
                            ArrayList<Word> buffer = getPrefixes(inputList.get(0));
                            
                                if(buffer.size() > 1 ){
                                    System.out.println("Error: More Than One Prefix Match.");
                                }
                                
                                else if(buffer.isEmpty()){
                                    System.out.println("Error: Unable To Find Exact or Prefix Match.");
                                }
                                
                                else{
                                    toCheck1 = buffer.get(0);
                                }
                        }
                        
                        
                        if(toCheck1 != null){
                           
                            runOneTermCommand(toCheck1);
                        }
                    }
                    
                    //procedure for two term command
                    else{
                        //check for exact matches for  input0 and input1
                        for(int i = 0; i<allWords.size(); i++){
                            if(allWords.get(i).getWord().equalsIgnoreCase(inputList.get(0))){
                                toCheck1 = allWords.get(i);
                            }
                            
                            if(allWords.get(i).getWord().equalsIgnoreCase(inputList.get(1))){
                                toCheck2 = allWords.get(i);
                            }
                        }
                        //toCheck1 is null if an exact match was not found
                        if(toCheck1 == null){
                            ArrayList<Word> buff = getPrefixes(inputList.get(0));
                            
                                if(buff.size() > 1 ){
                                    System.out.println("Error: More Than One Prefix Match For First Input.");
                                }
                                
                                else if(buff.isEmpty()){
                                    System.out.println("Error: Unable To Find Exact or Prefix Match For First Input.");
                                }
                                
                                else{
                                    toCheck1 = buff.get(0);
                                }
                        }
                        //toCheck2 is null if an exact match was not found
                        if(toCheck2 == null){
                            ArrayList<Word> buf = getPrefixes(inputList.get(1));
                            
                                if(buf.size() > 1 ){
                                    System.out.println("Error: More Than One Prefix Match For Second Input.");
                                }
                                
                                else if(buf.isEmpty()){
                                    System.out.println("Error: Unable To Find Exact or Prefix Match For Second Input.");
                                }
                                
                                else{
                                    toCheck2 = buf.get(0);
                                }
                        }
                        
                        
                        if(toCheck1 != null && toCheck2 != null){
                            runTwoTermCommand(toCheck1, toCheck2);
                        }
                        
                    }
                }
    
    }
                

	//grabs all Prefix type words in the list that begin with the input characters
        private ArrayList<Word> getPrefixes(String prefix){
            ArrayList<Word> toReturn = new ArrayList<>();
            
            for(int i = 0; i<listOfPrefixWords.size(); i++){
                if(listOfPrefixWords.get(i).getWord().startsWith(prefix.toLowerCase())){
                    toReturn.add(listOfPrefixWords.get(i));
                }
            }
            
            return toReturn;
        }
    
        //gets all prefix matches
	private ArrayList<Word> getAllWordsBeginningWith(String prefix){
            ArrayList<Word> toReturn = new ArrayList<>();
	
            for(int i = 0; i<allWords.size(); i++){ 
                if(allWords.get(i).getWord().startsWith(prefix.toLowerCase())){
                    toReturn.add(allWords.get(i));
                }

            }
            return toReturn;
	}
        
        
	
	private void runOneTermCommand(Word word){
        ArrayList <Terms> terms = allTerms;
        ArrayList <Terms> term = new ArrayList<>();
            for(int i = 0; i<terms.size(); i++){
                if(terms.get(i).termContainsWord(word)){
                    term.add(terms.get(i));
                }
            }
        execActionOneWord(term, word);
        }
	
	private void runTwoTermCommand(Word word1, Word word2){
            ArrayList <Terms> terms = allTerms;
            ArrayList <Terms> term1 = new ArrayList<>();
            ArrayList <Terms> term2 = new ArrayList<>();
            for(int i = 0; i<terms.size(); i++){
                if(terms.get(i).termContainsWord(word1)){
                    term1.add(terms.get(i)) ;
                }
                if(terms.get(i).termContainsWord(word2)){
                    term2.add(terms.get(i));
                }
            }
            execActionTwoWord(term1, term2, word1, word2);    
	}
        
        private void execActionOneWord(ArrayList<Terms> term, Word word1){
            ArrayList<Action> actions = allActions;
            for(int x = 0; x<actions.size(); x++){
                if(actions.get(x).getTermOne().termContainsWord(word1)){
                    actions.get(x).getMethod().doAction(game, word1, null);
                    queueEvent(Event.COMMAND);
                    return;
                }
            }
        }
        
        
        private void execActionTwoWord(ArrayList<Terms> term1, ArrayList<Terms> term2, Word word1, Word word2){
            ArrayList <Action> actions = allActions;
            Action chosenAction = null;
            for(int x = 0; x<actions.size(); x++){
                if(actions.get(x).getTermTwo() != null){
                    if(actions.get(x).getTermOne().termContainsWord(word1) ){
                         if(actions.get(x).getTermTwo().termContainsWord(word2) ) {
                             if(actions.get(x).getValidMethod() == null || actions.get(x).getValidMethod().isValid(game, word1, word2) ){
                                 if( chosenAction == null || actions.get(x).getPriority() > chosenAction.getPriority()){
                                    chosenAction = actions.get(x); 
                                 }
                             }
                            
                            
                         }
                    }
                }                
            }
            if(chosenAction!=null){
                chosenAction.getMethod().doAction(game, word1, word2);
                queueEvent(Event.COMMAND);
            }
            else{
                System.out.println("Error: No Applicable Action.");
            }
        }
        
        
        
        @Override
        public void queueEvent(Event e){
            eventQueue.add(e);
        }
}
