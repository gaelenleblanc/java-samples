
package cs345leblang.interpret;

/*
 * Enumeration giving types of events.
 * @author Gaelen LeBlanc (gaelen.leblanc@gmail.com)
 */

public enum Event {
	INIT, MOVE, COMMAND, EXIT
}
