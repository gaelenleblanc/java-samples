

package cs345leblang.interpret;

/**
 *
 * @author Gaelen LeBlanc
 */
public class StringMessage implements Message {
    private String msg;
    
    public StringMessage (String string){
        msg = string;
        
    }
    @Override
    public void print(GamePrintStream out){
        out.print(msg);
    }
    
    
    @Override
    public void printArgs(GamePrintStream out, String... args){
        out.print(msg);
    }
    
    @Override
    public void printAlt(GamePrintStream out, int alt){
        out.print(msg);
    }
}
