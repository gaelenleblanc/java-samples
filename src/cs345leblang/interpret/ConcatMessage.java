
package cs345leblang.interpret;
import java.util.*;
/**
 *
 * @author Gaelen LeBlanc
 */
public class ConcatMessage implements Message{
    private ArrayList<Message> messages = new ArrayList<>();
    
    public ConcatMessage(Message... msgs){
        messages.addAll(Arrays.asList(msgs));
    }
    
    @Override
    public void print(GamePrintStream out){
        for(Message msgs:messages){
            msgs.print(out);
        }
    }
    
    @Override
    public void printArgs(GamePrintStream out, String... args){
        for(Message msgs:messages){
            msgs.printArgs(out, args);
        }
    }
    
    @Override
    public void printAlt (GamePrintStream out, int alt){
        for(Message msgs:messages){
            msgs.printAlt(out, alt);
        }
    }
}
