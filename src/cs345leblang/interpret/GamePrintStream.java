
package cs345leblang.interpret;

/**
 * This is the interface provided for a "PrintStream" for a game.
 * 
 * @author Gaelen LeBlanc (gaelen.leblanc@gmail.com)
 */
public interface GamePrintStream {

	
	public void print(String s);
	
	
	public void println(String s);
	
	
	public void println();
	
	
	public void printf(String format, Object... args);
	
	
	public void flush();
}
