
package cs345leblang.interpret;
import cs345leblang.game.*;
/**
 *
 * @author Gaelen LeBlanc
 */
public class BoundSelectMessage implements Message {
    private Message msg;
    private Container obj;
   
    public BoundSelectMessage(Message wrapper,Container object){
        msg = wrapper;
        obj = object;
    }
    

    @Override
    public void print(GamePrintStream out){
        msg.printAlt(out, obj.getState());
    }

    @Override
    public void printArgs(GamePrintStream out, String... args){
        throw new UnsupportedOperationException("Method Not Supported.");

    }
    @Override
    public void printAlt(GamePrintStream out, int alt){
        msg.printAlt(out, obj.getState());
    }
}
