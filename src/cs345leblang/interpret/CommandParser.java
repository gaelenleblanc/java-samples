
package cs345leblang.interpret;

/**
 * This is the interface provided for a Command Parser.
 * 
 * @author Gaelen LeBlanc (gaelen.leblanc@gmail.com)
 */
public interface CommandParser {

   
    public boolean setExit(boolean exit);

    public void run();
    
    public void queueEvent(Event e);
}
