
package cs345leblang.interpret;

/**
 * This is the interface provided simple messages.
 * 
 * @author Gaelen LeBlanc (gaelen.leblanc@gmail.com)
 */
public interface Message {
	
	
	
	void print(GamePrintStream out);
	
	
	void printArgs(GamePrintStream out, String... args);
        
        void printAlt(GamePrintStream out, int alt);
}
