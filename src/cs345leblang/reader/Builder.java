

package cs345leblang.reader;

import java.io.InputStream;
import cs345leblang.game.*;
import cs345leblang.interpret.*;
/**
 * This is the interface provided for classes that build games.
 * 
 * @author Gaelen LeBlanc (gaelen.leblanc@gmail.com)
 */
public interface Builder {

    public void startBuild(InputStream in, GamePrintStream out);

    public Game buildComplete();

    public Word makeWord(String word, MatchType match);

    public Term makeTerm(String name);
    
    public Room makeRoom(String name, Message desc, Message brief);

    public void makePath(Term vocab, Room from, Room to);

    public void makeAction(Term vocab1, Term vocab2, int i, ValidMethod valid, ActionMethod action);

    public Player makePlayer(String name);

    public GameObject makeGameObject(String name, Term vocab,
            Message inventoryDesc, Message hereIsDesc, Message longDesc);
    
    public Message makeMessage(String msg);
    
    public Message makeMessage(Message... msgs);
    
    public Message makeArgMessage(int index);
    
    public Message makeCycleMessage(Message... msgs);
    
    public Message makeAltMessage(Message... msgs);
    
    public void makeHandler(Event event, HandlerMethod handler);
}