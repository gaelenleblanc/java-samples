/**
 * This work is licensed under the Creative Commons Attribution 3.0
 * Unported License. To view a copy of this license, visit
 * http://creativecommons.org/licenses/by/3.0/ or send a letter to
 * Creative Commons, 444 Castro Street, Suite 900,
 * Mountain View, California, 94041, USA. 
 */

package cs345leblang.reader;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import cs345leblang.game.*;
import cs345leblang.interpret.*;


/**
 * This class builds a game description for the game
 * "Cloak of Darkness".
 * 
 * @author Chris Reedy (Chris.Reedy@wwu.edu)
 */
public class CloakHardCoded implements GameDescription {
	
	private Builder builder;
	
	private static class WordData {
		MatchType m;
		Word word;
		
		WordData(MatchType m, Word word) {
			this.m = m;
			this.word = word;
		}
	}

	/* wordMap is a used inside the HardCodedGame to track constructed
	 * words. This is used to keep build() from building the same word
	 * multiple times. wordMap is private to the HardCodedGame class.
	 */
	private Map<String, WordData> wordMap;
	
	public CloakHardCoded(Builder builder) {
		this.builder = builder;
	}
	
	private Word makeWord(String w, MatchType m) {
		WordData wData = wordMap.get(w);
		if (wData != null) {
			assert wData.m == m : "Differing MatchTypes for " + w; 
			return wData.word;
		}
		Word newWord = builder.makeWord(w, m);
		wordMap.put(w, new WordData(m, newWord));
		return newWord;
	}
	
	private void addWords(Term vocab, MatchType match, String... words) {
		for (String w : words) {
			Word word = makeWord(w, match);
			vocab.addWord(word);
		}
	}
	
    Map<String, Message> messageMap = new HashMap<String, Message>();
	
    private Message _m(String smsg) {
        Message msg = messageMap.get(smsg);
		if (msg != null)
			return msg;
		msg = builder.makeMessage(smsg);
		messageMap.put(smsg, msg);
		return msg;
	}
	
    private Message _m(Message... msgs) {
        return builder.makeMessage(msgs);
    }
    
    private Message _sm(Message... msgs) {
        return builder.makeAltMessage(msgs);
    }
    
    private Message _am(String smsg) {
        Message msg = messageMap.get(smsg);
		if (msg != null)
			return msg;
        /* At this point scan the message looking for % markers.
		 * A %n (n in 1 .. 9) indicates that an argument is to be
		 * substituted at this point.
         * A %% substitutes a percent sign.
		 * 
         * XXX This does not allow for more than 9 arguments other niceties.
		 */
        List<Message> msgParts = new ArrayList<Message>();
		int b = 0;
		for (int i = 0; i < smsg.length(); ++i) {
			if (smsg.charAt(i) != '%')
				continue;
			/* Found a % sign. Insert substitution.
			 * XXX No check for exceeding string length, etc. is made.
			 */
			msgParts.add(_m(smsg.substring(b, i)));
			i += 1;
			b = i + 1;
            char indicator = smsg.charAt(i);
            if (indicator == '%')
                msgParts.add(_m("%"));
            else
                msgParts.add(builder.makeArgMessage(indicator - '1'));
		}
		if (b < smsg.length())
			msgParts.add(_m(smsg.substring(b, smsg.length())));
        msg = builder.makeMessage(msgParts.toArray(new Message[msgParts.size()]));
		return msg;
	}
	
	/* Here begins the actual game. */
	
	static Message mDarkHere;
	static boolean messageRead;
	
	private static void describeHere(Game game) {
		Room here = game.thePlayer.getLocation();
		if (here.getProp("lit").equals("lit")) {
			if (game.thePlayer.getProp("verbose").equals("no") &&
					here.getProp("beenhere").equals("yes")) {
				here.getBriefDescription().print(game.messageOut);
			} else {
				here.getDescription().print(game.messageOut);
			}
			for (GameObject item : here.getContents()) {
			    game.messageOut.print(" ");
				item.getHereIsDesc().print(game.messageOut);
			}
		} else {
			mDarkHere.print(game.messageOut);
		}
	}
	
    /**
     * This method locates an object by name in allObjects.
     * @param game the game object for the game
     * @param w a word designating the object
     * @return the found object or null if no object was found.
     */
    private static GameObject findObject(Game game, Word w) {
        for (GameObject it : game.allObjects) {
 			if (it.match(w)) {
				return it;
			}
		}
		return null;
	}

	@Override
	public Game build(InputStream in, GamePrintStream out) {
		
		/* wordMap is needed for all versions of hardcoded game. */
		wordMap = new HashMap<String, WordData>();

		/* Call the builder to start the build. */
		builder.startBuild(in, out);
		
		Term noiseWords = builder.makeTerm("noisewords");
		addWords(noiseWords, MatchType.PREFIX, "a", "an", "the", "and", "it",
				"that", "this", "to", "at", "with", "room");
		
		/* Define some vocabulary.
		 */
		final Term vVerbs = builder.makeTerm("verbs");
		final Term vObjects = builder.makeTerm("objects");
		final Term vMove = builder.makeTerm("go");
		addWords(vMove, MatchType.PREFIX, "go", "walk", "proceed");
		addWords(vVerbs, MatchType.PREFIX, "go", "walk", "proceed");
		final Term vNorth = builder.makeTerm("north");
		addWords(vNorth, MatchType.PREFIX, "north");
		addWords(vObjects, MatchType.PREFIX, "north");
		final Term vSouth = builder.makeTerm("south");
		addWords(vSouth, MatchType.PREFIX, "south");
		addWords(vObjects, MatchType.PREFIX, "south");
		final Term vEast = builder.makeTerm("east");
		addWords(vEast, MatchType.PREFIX, "east");
		addWords(vObjects, MatchType.PREFIX, "east");
		final Term vWest = builder.makeTerm("west");
		addWords(vWest, MatchType.PREFIX, "west");
		addWords(vObjects, MatchType.PREFIX, "west");
		final Term vLook = builder.makeTerm("look");
		addWords(vLook, MatchType.PREFIX, "look", "examine");
		addWords(vVerbs, MatchType.PREFIX, "look", "examine");
		final Term vRead = builder.makeTerm("read");
		addWords(vRead, MatchType.PREFIX, "read");
		addWords(vVerbs, MatchType.PREFIX, "read");
		final Term vInventory = builder.makeTerm("inventory");
		addWords(vInventory, MatchType.PREFIX, "inventory");
		addWords(vVerbs, MatchType.PREFIX, "inventory");
		final Term vGet = builder.makeTerm("get");
		addWords(vGet, MatchType.PREFIX, "get");
		addWords(vVerbs, MatchType.PREFIX, "get");
		final Term vDrop = builder.makeTerm("drop");
		addWords(vDrop, MatchType.PREFIX, "drop");
		addWords(vVerbs, MatchType.PREFIX, "drop");
		final Term vHang = builder.makeTerm("hang");
		addWords(vHang, MatchType.PREFIX, "hang");
		addWords(vVerbs, MatchType.PREFIX, "hang");
		final Term vQuit = builder.makeTerm("quit");
		addWords(vQuit, MatchType.PREFIX, "quit");
		final Term vAround = builder.makeTerm("around");
		addWords(vAround, MatchType.PREFIX, "around");
		addWords(vObjects, MatchType.PREFIX, "around");
		
		final Message mIntro = _m("The Cloak of Darkness (Version 0.1)\n\n" +
								   "Hurrying through the rain swept November night, " +
								   "you're glad to see the bright lights of the Opera House. " +
								   "It's surprising that there aren't more people about; but, " +
								   "what do you expect in a CS assignment ...?\n\n");
		
		builder.makeAction(vQuit, null, 0, null, new ActionMethod() {
			@Override
			public void doAction(Game game, Word w1, Word w2) {
				game.parser.queueEvent(Event.EXIT);
			}
		});
				
		final Room rHall = builder.makeRoom("hall",
				_m("You are standing in a spacious hall, splendidly decorated in red and gold, with glittering chandeliers overhead. The entrance from the street is to the north, and there are doorways south and west."),
				_m("You are in the hall."));
		final Room rCloakRoom = builder.makeRoom("cloakroom",
				_m("The walls of this small room were clearly once lined with hooks, though now only one remains. The exit is a door to the east."),
				_m("You are in the cloak room."));
		final Room rBar = builder.makeRoom("bar",
				_m("The bar, much rougher than you'd have guessed after the opulence of the foyer to the north, is completely empty."),
				_m("You are in the bar."));

		final Term vNorthOutHall = builder.makeTerm("northouthall");
		addWords(vNorthOutHall, MatchType.PREFIX, "north", "out", "hall");
		final Term vNorthOut = builder.makeTerm("northout");
		addWords(vNorthOut, MatchType.PREFIX, "north", "out");
		builder.makePath(vNorthOutHall, rBar, rHall);
		final Term vEastOutHall = builder.makeTerm("eastouthall");
		addWords(vEastOutHall, MatchType.PREFIX, "east", "out", "exit", "hall");
		builder.makePath(vEastOutHall, rCloakRoom, rHall);
		builder.makePath(vWest, rHall, rCloakRoom);
		builder.makePath(vSouth, rHall, rBar);
		
		addWords(vObjects, MatchType.PREFIX, "out", "exit", "hall");
		
		/* Initialize properties for rooms. */
		builder.makeHandler(Event.INIT, new HandlerMethod() {
			@Override
			public void doHandler(Game game, Event evt) {
				rHall.setProp("lit", "lit");
				rHall.setProp("beenhere", "no");
				rCloakRoom.setProp("lit", "lit");
				rCloakRoom.setProp("beenhere", "no");
				rBar.setProp("lit", "dark");
				rBar.setProp("beenhere", "no");
				
				messageRead = false;
			}
		});
		
		final Term vDirect = builder.makeTerm("direction");
		addWords(vDirect, MatchType.PREFIX, "north", "south", "east", "west", "out", "exit", "hall");

		final Term vVerbose = builder.makeTerm("verbose");
		addWords(vVerbose, MatchType.PREFIX, "verbose");
		final Word wYes = makeWord("yes", MatchType.PREFIX);
		final Word wNo = makeWord("no", MatchType.PREFIX);
		final Term vYesNo = builder.makeTerm("yesno");
		vYesNo.addWord(wYes);
		vYesNo.addWord(wNo);
		
		builder.makeAction(vVerbose, vYesNo, 0, null, new ActionMethod() {
			@Override
			public void doAction(Game game, Word w1, Word w2) {
				if (w2 == wYes)
					game.thePlayer.setProp("verbose", "yes");
				else
					game.thePlayer.setProp("verbose", "no");
			}		
		});
		
		final Message mDontKnowHowToMove = _am("I don't know how to move %1.");
		builder.makeAction(vMove, vDirect, 0, null, new ActionMethod() {
			@Override
			public void doAction(Game game, Word w1, Word w2) {
				if (game.thePlayer.moveOnPath(w2)) {
					// The player moved.
					describeHere(game);
					game.thePlayer.getLocation().setProp("beenhere", "yes");
				} else
					// Player could not move
					mDontKnowHowToMove.printArgs(game.messageOut, w2.getWord());
			}
		});
		
		final Message mJustArrived =_m("You've only just arrived, and besides, the weather outside seems to be getting worse.");
		
		builder.makeAction(vMove, vNorthOut, 10, new ValidMethod() {
			@Override
			public boolean isValid(Game game, Word w1, Word w2) {
				return game.thePlayer.getLocation() == rHall;
			}
		}, new ActionMethod() {
			@Override
			public void doAction(Game game, Word w1, Word w2) {
				mJustArrived.print(game.messageOut);
			}
		});
		
		builder.makeAction(vLook, null, 0, null, new ActionMethod() {
			@Override
			public void doAction(Game game, Word w1, Word w2) {
				String beenHere = game.thePlayer.getLocation().getProp("beenhere");
				game.thePlayer.getLocation().setProp("beenhere", "no");
				describeHere(game);
				game.thePlayer.getLocation().setProp("beenhere", beenHere);
			}
		});
		
		builder.makeAction(vLook, vAround, 0, null, new ActionMethod() {
			@Override
			public void doAction(Game game, Word w1, Word w2) {
				String beenHere = game.thePlayer.getLocation().getProp("beenhere");
				game.thePlayer.getLocation().setProp("beenhere", "no");
				describeHere(game);
				game.thePlayer.getLocation().setProp("beenhere", beenHere);
			}
		});
		
		final Term vItems = builder.makeTerm("items");
		
		final Term vHook = builder.makeTerm("hookvocab");
		addWords(vHook, MatchType.PREFIX, "hook", "peg");
		addWords(vItems, MatchType.PREFIX, "hook", "peg");
		addWords(vObjects, MatchType.PREFIX, "hook", "peg");
		final GameObject iHook = builder.makeGameObject("hook", vHook,
				_m("a brass hook"),
				_m("There is a brass hook here."),
				_m(_m("It's just a small brass hook, screwed into the wall."),
					_sm(_m(""), _m(" Your velvet cloak is now hung upon it."))));

		final Term vCloak = builder.makeTerm("cloakvocab");
		addWords(vCloak, MatchType.PREFIX, "cloak", "velvet");
		addWords(vItems, MatchType.PREFIX, "cloak", "velvet");
		addWords(vObjects, MatchType.PREFIX, "cloak", "velvet");
		final GameObject iCloak = builder.makeGameObject("cloak", vCloak,
				_m(_m("a velvet cloak"), _sm(_m(" (worn)"), _m(""))),
				_m("A black velvet cloak hangs on the hook."),
				_m("It is a handsome cloak, of velvet trimmed with satin, and slightly spattered with raindrops. Its blackness is so deep that it almost seems to suck light from the room."));
		
		final Term vMessage = builder.makeTerm("messagevocab");
		addWords(vMessage, MatchType.PREFIX, "message", "sawdust", "floor");
		addWords(vItems, MatchType.PREFIX, "message", "sawdust", "floor");
		addWords(vObjects, MatchType.PREFIX, "message", "sawdust", "floor");
		final GameObject iMessage = builder.makeGameObject("message", vMessage,
				_m(""),
				_m("There seems to be some sort of message scrawled in the sawdust on the floor."),
				_m(_m("The message "),
				   _sm(_m("has been carelessly trampled, making it difficult to read. You can just distinguish the words"),
					   _m("neatly marked in the sawdust reads")),
				   _m("...\n\n"),
				   _m("YOU HAVE "), _sm(_m("LOST"), _m("WON")), _m(" !!!\n")));
		
		/* Properties and locations for game items. */
		builder.makeHandler(Event.INIT, new HandlerMethod() {
			@Override
			public void doHandler(Game game, Event evt) {
				rCloakRoom.addObject(iHook);
				game.thePlayer.addObject(iCloak);
				rBar.addObject(iMessage);

				iHook.setState(0);
				iCloak.setState(0);
				iMessage.setState(4);
			}
		});
		
		final Message mPartingShot = _sm(
				_m("I can see why you want to quit.\n"),
				_m("You figured it out too late I see!\n"),
				_m("As you wish! ...\n"),
				_m("Giving up so easily ... you've almost won.\n")
				);
		
		/* Exit handler, print an appropriate message. */
		builder.makeHandler(Event.EXIT, new HandlerMethod() {
			@Override
			public void doHandler(Game game, Event evt) {
				game.parser.setExit(true);
				if (messageRead) {
					iMessage.getLongDesc().print(game.messageOut);				
				} else {
					int option = 0;
					if (iCloak.getState() > 0) {
						// Cloak is on hook
						option += 1;
					}
					if (iMessage.getState() > 1) {
						// Message can still be read
						option += 2;
					}
					mPartingShot.printAlt(game.messageOut, option);
				}
			}
		});

		final Message mAlreadyHung = _m("The cloak is already hung up!");
		final Message mAlreadyWearing = _m("You're already wearing the cloak!");
		final Message mHangingCloak = _m("You hang the cloak on the hook.");
		final Message mWearingCloak = _m("You are wearing the cloak.");
		
		final Message mBetterIdea = _m("I have a better idea: let's get out of here before you disturb anything.");
		mDarkHere = _m("It is dark in here!");
		final Message mNothingToRead = _m("There's nothing here one could read!");
		
		final Message mNotCarryingItem = _am("I see no %1 here!.");
		final Message mNotCarryingAnything = _m("You are empty-handed.");
		final Message mCarryingStart = _m("You are carrying ");
		final Message mCarryingAnd = _m(" and ");
		final Message mCarryingSep = _m(", ");
		final Message mCarryingSepAnd = _m(", and ");
		final Message mCarryingFinal = _m(".");
		
		builder.makeAction(vHang, vCloak, 0, new ValidMethod () {
			@Override
			public boolean isValid(Game game, Word w1, Word w2) {
				return game.thePlayer.getLocation() == rCloakRoom;
			}
		}, new ActionMethod() {
			@Override
			public void doAction(Game game, Word w1, Word w2) {
				if (iCloak.getState() == 1) {
					mAlreadyHung.print(game.messageOut);
					return;
				}
				iCloak.setState(1);
				iHook.setState(1);
				game.thePlayer.removeObject(iCloak);
				rCloakRoom.addObject(iCloak);
				rBar.setProp("lit", "lit");
				mHangingCloak.print(game.messageOut);
			}
		});
		
		builder.makeAction(vGet, vCloak, 0, new ValidMethod () {
			@Override
			public boolean isValid(Game game, Word w1, Word w2) {
				return game.thePlayer.getLocation() == rCloakRoom;
			}
		}, new ActionMethod() {
			@Override
			public void doAction(Game game, Word w1, Word w2) {
				if (iCloak.getState() == 0) {
					mAlreadyWearing.print(game.messageOut);
					return;
				}
				iCloak.setState(0);
				iHook.setState(0);
				rCloakRoom.removeObject(iCloak);
				game.thePlayer.addObject(iCloak);
				rBar.setProp("lit", "dark");
				mWearingCloak.print(game.messageOut);
			}
		});
		
		builder.makeAction(vLook, vItems, 0, null, new ActionMethod() {
			@Override
			public void doAction(Game game, Word w1, Word w2) {
				GameObject item = findObject(game, w2);
				assert (item != null);
				Room here = game.thePlayer.getLocation();
				if (game.thePlayer.contains(item) || here.contains(item)) {
					if (here.getProp("lit").equals("lit")) {
						item.getLongDesc().print(game.messageOut);
					} else {
						mDarkHere.print(game.messageOut);
					}
				} else {
					mNotCarryingItem.printArgs(game.messageOut, w2.getWord());
				}
			}
		});
		
		builder.makeAction(vInventory, null, 0, null, new ActionMethod() {
			@Override
			public void doAction(Game game, Word w1, Word w2) {
				Collection<GameObject> c = game.thePlayer.getContents();
				if (c.isEmpty()) {
					mNotCarryingAnything.print(game.messageOut);
				} else if (c.size() <= 2) {
					mCarryingStart.print(game.messageOut);
					Iterator<GameObject> iter = c.iterator();
					GameObject item1 = iter.next();
					item1.getInventoryDesc().print(game.messageOut);
					if (c.size() == 2) {
						mCarryingAnd.print(game.messageOut);
						GameObject item2 = iter.next();
						item2.getInventoryDesc().print(game.messageOut);
					}
					mCarryingFinal.print(game.messageOut);
				} else {
					int itemNo = 1;
					for (GameObject item : c) {
						if (itemNo == 1)
							mCarryingStart.print(game.messageOut);
						else if (itemNo == c.size())
							mCarryingSepAnd.print(game.messageOut);
						else
							mCarryingSep.print(game.messageOut);
						item.getInventoryDesc().print(game.messageOut);
						itemNo += 1;
					}
					mCarryingFinal.print(game.messageOut);
				}
			}
		});
		
		final ValidMethod inBar = new ValidMethod() {
			@Override
			public boolean isValid(Game game, Word w1, Word w2) {
				return game.thePlayer.getLocation() == rBar;
			}
		};
		
		final ActionMethod readMessage = new ActionMethod() {
			@Override
			public void doAction(Game game, Word w1, Word w2) {
				/* The player is in the Bar. */
				if (rBar.getProp("lit").equals("dark")) {
					mDarkHere.print(game.messageOut);
					game.messageOut.print(" ");
					mBetterIdea.print(game.messageOut);
				} else {
					/* In the bar without the cloak. */
					messageRead = true;
					game.parser.queueEvent(Event.EXIT);
				}
			}			
		};
		
		final ActionMethod nothingToRead = new ActionMethod() {
			@Override
			public void doAction(Game game, Word w1, Word w2) {
				mNothingToRead.print(game.messageOut);
			}
		};
		
		builder.makeAction(vLook, vMessage, 10, inBar, readMessage);
		builder.makeAction(vRead, vMessage, 10, inBar, readMessage);
		builder.makeAction(vRead, null, 10, inBar, readMessage);
		builder.makeAction(vRead, vMessage, 0, null, nothingToRead);
		builder.makeAction(vRead, null, 0, null, nothingToRead);
	
		final ValidMethod inDarkBar = new ValidMethod() {
			@Override
			public boolean isValid(Game game, Word w1, Word w2) {
				return game.thePlayer.getLocation() == rBar && rBar.getProp("lit").equals("dark");
			}
		};
		final ActionMethod betterIdea = new ActionMethod() {
			@Override
			public void doAction(Game game, Word w1, Word w2) {
				mBetterIdea.print(game.messageOut);
			}
		};
		
		builder.makeHandler(Event.COMMAND, new HandlerMethod() {
			@Override
			public void doHandler(Game game, Event evt) {
				if (game.thePlayer.getLocation() == rBar && rBar.getProp("lit").equals("dark")) {
					int state = iMessage.getState();
					if (state > 0) {
						iMessage.setState(state - 1);
					}
					
				}
			}
		});
		
		builder.makeAction(vVerbs, null, -10, inDarkBar, betterIdea);
		builder.makeAction(vVerbs, vObjects, -10, inDarkBar, betterIdea);
	
		/* Create and position player and initialize player properties. */
		builder.makePlayer("You");
		
		builder.makeHandler(Event.INIT, new HandlerMethod() {
			@Override
			public void doHandler(Game game, Event evt) {
				game.thePlayer.apportTo(rHall);
				game.thePlayer.setProp("verbose", "no");
			}
		});
		
		/* Final initialization and start. */

		builder.makeHandler(Event.INIT, new HandlerMethod() {
			@Override
			public void doHandler(Game game, Event evt) {
				/* Introductory message and start game. */
				mIntro.print(game.messageOut);
				describeHere(game);
				game.thePlayer.getLocation().setProp("beenhere", "yes");				
			}
		});
		
		/* Call the builder to finish the build. */
		return builder.buildComplete();

	}
}
