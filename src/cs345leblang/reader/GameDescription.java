/**
 * This work is licensed under the Creative Commons Attribution 3.0
 * Unported License. To view a copy of this license, visit
 * http://creativecommons.org/licenses/by/3.0/ or send a letter to
 * Creative Commons, 444 Castro Street, Suite 900,
 * Mountain View, California, 94041, USA. 
 */

package cs345leblang.reader;

import java.io.InputStream;

import cs345leblang.game.Game;
import cs345leblang.interpret.GamePrintStream;

/**
 * This is the interface that must be implemented by classes that build games.
 * 
 * 
 * @author Chris Reedy (Chris.Reedy@wwu.edu)
 */
public interface GameDescription {

	
	public Game build(InputStream in, GamePrintStream out);
	
}
