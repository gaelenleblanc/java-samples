
package cs345leblang.game;

/**
 * This is the interface defines an Valid method which is used to test
 * whether an action is valid or not.
 * 
 * @author Gaelen LeBlanc (gaelen.leblanc@gmail.com)
 */
public interface ValidMethod {

	public boolean isValid(Game game, Word w1, Word w2);
	
}
