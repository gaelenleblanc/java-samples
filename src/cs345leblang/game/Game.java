

package cs345leblang.game;

import java.util.*;
import java.io.*;
import cs345leblang.interpret.*;

/**
 * This class contains the global objects for a game.
 * 
 * @author Gaelen LeBlanc (gaelen.leblanc@gmail.com)
 */
public class Game {
		

        public Collection<GameObject> allObjects = new HashSet<GameObject>();

    
	public static Player thePlayer;

	public boolean exit = false;
 
	public CommandParser parser;

	public static InputStream commandIn;

	public static GamePrintStream messageOut;
	
	public static Terms noiseWords = new Terms("noisewords");
        
        private ArrayList<Handler> allHandlers = new ArrayList<>();
	
	private ArrayList<Action> allActions = new ArrayList<>();
	
	private ArrayList<Terms> allTerms = new ArrayList<>();
	
	private ArrayList<Room> allRooms = new ArrayList<>();
	
	private ArrayList<Word> allWords = new ArrayList<>();
	
	private ArrayList<Word>  listOfExactWords = new ArrayList<>();
	
	private ArrayList<Word> listOfPrefixWords = new ArrayList<>();
        
        private ArrayList<Message> allMessages = new ArrayList<>();
        
        public void addHandler(Handler handler){
            allHandlers.add(handler);
        }
        
        public ArrayList<Handler> getAllHandlers(){
            return allHandlers;
        }

        
        public void addMessage(Message msg){
            allMessages.add(msg);
        }
        public ArrayList getAllMessages(){
            return allMessages;
        }
        
        public void addAction(Action action){
            allActions.add(action);
        }
        
        public void addTerm(Terms term){
            allTerms.add(term);
        }
        
        public void addRoom(Room room){
            allRooms.add(room);
        }
        
        public void addWord(Word word){
            allWords.add(word);
        }
        
        public void addPrefixWord(Word word){
            listOfPrefixWords.add(word);
        }
        
        public void addExactWord(Word word){
            listOfExactWords.add(word);
        }
        
        public ArrayList<Word> getPrefixWords(){
            return listOfPrefixWords;
        }
        
        public ArrayList<Word> getExactWords(){
            return listOfExactWords;
        }
        
        public ArrayList<Room> getAllRooms(){
            return allRooms;
        }
        
        public ArrayList<Terms> getAllTerms(){
            return allTerms;
        }
        
        public ArrayList<Action> getallActions(){
            return allActions;
        }
        
        public ArrayList<Word> getAllWords(){
            return allWords;
        }
	
	public Game() {
               //Nothing to do
               
                
	
	}

}
