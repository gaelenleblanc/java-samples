
package cs345leblang.game;
import java.util.*;
import cs345leblang.interpret.*;

/**
 * This is the interface provided for Players.
 * 
 * @author Gaelen LeBlanc (gaelen.leblanc@gmail.com
 */
public class ThePlayer implements Player{
	private Room location;
	private ArrayList <GameObject> inventory = new ArrayList<>();
	private String name;
        private int state;
        private HashMap<String, String> properties = new HashMap<>();
	
	public ThePlayer(String name){
		this.name = name;
                state = 0;
	}

        @Override
        public void addObject(GameObject obj){
		inventory.add(obj);
	}

        @Override
	public void removeObject(GameObject obj){
		inventory.remove(obj);
	}

        @Override
	public boolean contains(GameObject obj){
          return inventory.contains(obj);
	}

        @Override
	public Collection <GameObject> getContents(){
		return inventory;
	}

        @Override
	public Room getLocation(){
		return this.location;
	}

        @Override
	public void apportTo(Room room){
		location = room;
	}

        @Override
	public void startAt(Room room){
		location = room;
	}

        @Override
	public boolean moveOnPath(Word pathName){
                Room destination = location;
                for(Path p: location.getPaths()){
                    if(p.getIdentifier().termContainsWord(pathName)){
                        destination = p.getDestinationRoom();
                        apportTo(destination);
                        return true;
                    }
                }
                return false;
	}
	
        @Override
	public void lookAround(){
            Message toPrint = location.getDescription();
            System.out.println(toPrint.toString());
            
            for(GameObject o:location.getContents()){
                System.out.println(o.getHereIsDesc().toString());
            }
        }
                
        @Override
        public int getState(){
            return state;
        }
        @Override
        public void setState(int i){
            state = i;
        }
        @Override
        public String getProp(String propName){
            if(properties.containsKey(propName)){
                return properties.get(propName);
            }
            return "";
        }
        
        @Override
        public void setProp(String propName, String value){
            properties.put(propName, value);

        }



}	