package cs345leblang.game;

/*
 *@author Gaelen LeBlanc (gaelen.leblanc@gmail.com)
*/

public class Path{
	private Term identifiers;
	private Room initialRoom;
	private Room destinationRoom;
	
	public Path (Term identifier, Room initial, Room destination){
		identifiers = identifier;
		initialRoom = initial;
		destinationRoom = destination;
	}
	
	public Term getIdentifier(){
		return this.identifiers;
	}
	
	public Room getInitialRoom(){
		return initialRoom;
	}
	
	public Room getDestinationRoom(){
		return destinationRoom;
	}
	
	
	
}