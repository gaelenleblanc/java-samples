

package cs345leblang.game;

import java.util.Collection;

/**
 * This is the interface provided for Containers.
 * 
 * @author Gaelen LeBlanc   (gaelen.leblanc@gmail.com)
 */

public interface Container {
    
    
    public void addObject(GameObject obj);
	
    public void removeObject(GameObject obj);
	
    public boolean contains(GameObject obj);
	
    public Collection<GameObject> getContents();

    public int getState();

    public void setState(int i);

    public String getProp(String propName);

    public void setProp(String propName, String value);
}
