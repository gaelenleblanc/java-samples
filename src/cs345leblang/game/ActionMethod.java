

package cs345leblang.game;

/**
 * This is the interface defines an Action method which is the
 * method for an Action.
 * 
 * @author Gaelen LeBlanc (gaelen.leblanc@gmail.com)
 */
public interface ActionMethod {

	public void doAction(Game game, Word w1, Word w2);
}
