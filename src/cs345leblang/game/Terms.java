

package cs345leblang.game;
import java.util.*;

/*
 * @author Gaelen LeBlanc (gaelen.leblanc@gmail.com)
*/


 public class Terms implements Term{
	private ArrayList<Word> words = new ArrayList<>(); 
	private String nameOfTerm;
	
	 public Terms (String name){
		nameOfTerm = name;
	 }
	 

	@Override
	public void addWord(Word word){
		words.add(word);	
	}
        @Override
        public String getTermName(){
            return nameOfTerm;
        }
        
        public boolean termContainsString(String string){
            String temp;
            for(int i = 0; i<words.size(); i++){
                temp = words.get(i).getWord();
                if(temp.equals(string)){
                    return true;
                }
            }
            return false;
        }
        
        public boolean termHasPrefixString(String string){
            String temp;
            for(int i = 0; i<words.size(); i++){
                temp = words.get(i).getWord();
                if(temp.startsWith(string.toLowerCase() )){
                    return true;
                }
            }
            return false;
        }
        
	
        @Override
	public boolean termContainsWord(Word word){
		for(int i = 0; i < words.size(); i++){
			if(words.get(i).equals(word)){
				return true;
			}
		}
		return false;
	}
	
 }