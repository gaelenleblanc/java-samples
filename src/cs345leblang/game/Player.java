
package cs345leblang.game;

/**
 * This is the interface provided for Players.
 * 
 * @author Gaelen LeBlanc (gaelen.leblanc@gmail.com)
 */
public interface Player extends Container {

	
	public Room getLocation();
	
	
	public void apportTo(Room room);

	
	public void startAt(Room room);

	public boolean moveOnPath(Word pathName);

	public void lookAround();

}
