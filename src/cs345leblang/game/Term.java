

package cs345leblang.game;

/**
 * This is the interface provided for Terms.
 * 
 * @author Gaelen LeBlanc (gaelen.leblanc@gmail.com)
 */
public interface Term {
	
	public void addWord(Word word);
        
        public boolean termContainsWord(Word word);
        
        public String getTermName();
}
