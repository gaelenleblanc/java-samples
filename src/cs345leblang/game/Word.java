
package cs345leblang.game;

/**
 * This is the interface provided for words.
 * 
 * @author Gaelen LeBlanc (gaelen.leblanc@gmail.com)
 */
public interface Word {
	
    public String getWord();
    
    public MatchType getMatchType();
        
	
}
