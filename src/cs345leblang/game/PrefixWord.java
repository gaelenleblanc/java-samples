
package cs345leblang.game;

/**
 * @author Gaelen LeBlanc (gaelen.leblanc@gmail.com)
 */
 public class PrefixWord implements Word{
	
	private String word;
	
	private MatchType PREFIX;
	
	public PrefixWord(String input){
		word = input;
		
	}
	@Override
	public String getWord(){
		return this.word;
	}
	@Override
	public MatchType getMatchType(){
		return MatchType.PREFIX;
		
	}
	
 }