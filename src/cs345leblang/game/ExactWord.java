

package cs345leblang.game;

/**
 * @author Gaelen LeBlanc (gaelen.leblanc@gmail.com)
 */
 public class ExactWord implements Word {
	
	private String word;
	
	private MatchType EXACT;

	public ExactWord (String input){
		word = input;
	}
	@Override
	public String getWord(){
		return this.word;
	}
	@Override
	public MatchType getMatchType(){
		return MatchType.EXACT;
	}
 }