
package cs345leblang.game;

import java.io.*;
import java.util.*;

import cs345leblang.reader.Builder;
import cs345leblang.interpret.*;


/**
 * This class is an implementation of a Builder for the game.
 * 
 * @author Gaelen LeBlanc (gaelen.leblanc@gmail.com)
 */
public class GameBuilder implements Builder {
    
                Game game;
	
	
	@Override public void startBuild(InputStream in, GamePrintStream out) {
		game = new Game();
                Game.commandIn = in;
                Game.messageOut = out;
	}
	
	
	@Override public Game buildComplete() {
		return game;
	}
	
	@Override public Word makeWord(String word, MatchType match) {
                Word toReturn;
		
		if(match.equals(MatchType.EXACT)){
			toReturn = new ExactWord(word);
                        game.addExactWord(toReturn);
		}
		else{
			toReturn = new PrefixWord(word);
                        game.addPrefixWord(toReturn);
		}
                
		game.addWord(toReturn);
		return toReturn;
	}
	
	
	@Override
	public Term makeTerm(String name) {
            Terms toReturn;
            if(name.equals("noisewords")){
		toReturn = Game.noiseWords;
		return toReturn;
            }
            else{
		toReturn = new Terms(name);
            }
            game.addTerm(toReturn);
            return toReturn;
	}
	
	
	@Override
	public Room makeRoom(String name, Message desc, Message brief) {
            Room room = new BaseRoom(name, desc, brief);
            Message descBoundSelectMessage = new BoundSelectMessage(desc, room);
            Message briefBoundSelectMessage = new BoundSelectMessage(brief, room);
            Room toReturn = new BaseRoom(name, descBoundSelectMessage, briefBoundSelectMessage);
            game.addRoom(toReturn);
            return toReturn;
	}
	
	@Override
	public void makePath(Term vocab, Room from, Room to) {
            Path newPath = new Path(vocab, from, to);
            from.addPath(newPath);
		
	}

	@Override
	public void makeAction(Term vocab1, Term vocab2, int i,ValidMethod valid, ActionMethod action) {
            Action toReturn = new Action(vocab1, vocab2,i,valid, action);		
            game.addAction(toReturn);
	}

	@Override
	public Player makePlayer(String name) {
            Player toReturn = new ThePlayer(name);
            Game.thePlayer = toReturn; 
            return Game.thePlayer;
	}
	
	@Override
	public GameObject makeGameObject(String name, Term vocab,
                    Message inventoryDesc, Message hereIsDesc, Message longDesc) {
			
            GameObjects obj = new GameObjects(name, vocab, inventoryDesc, hereIsDesc, longDesc);
            game.allObjects.add(obj);
            return obj;
	}

        @Override
        public Message makeMessage(String msg) {
            StringMessage toReturn = new StringMessage(msg);
            game.addMessage(toReturn);
            return toReturn;
        }


       @Override
       public Message makeMessage(Message... msgs) {
           ConcatMessage toReturn = new ConcatMessage(msgs);
           game.addMessage(toReturn);
           return toReturn;
        }

        @Override
        public Message makeArgMessage(int index) {
            ArgMessage toReturn = new ArgMessage(index);
            game.addMessage(toReturn);
        return toReturn;
        }

        @Override
        public Message makeCycleMessage(Message... msgs) {
            CycleMessage toReturn = new CycleMessage(msgs);
            game.addMessage(toReturn);
            return toReturn;
        }
        
        @Override
        public Message makeAltMessage(Message... msgs){
            SelectMessage toReturn = new SelectMessage(msgs);
            game.addMessage(toReturn);
            return toReturn;
        }
        
        @Override
        public void makeHandler(Event event, HandlerMethod handler){
            Handler handle = new Handler(event, handler);
            game.addHandler(handle);
        }
}
