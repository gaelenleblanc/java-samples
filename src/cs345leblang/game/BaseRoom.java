
package cs345leblang.game;
import java.util.*;
import cs345leblang.interpret.*;
/**
 * @author Gaelen LeBlanc (gaelen.leblanc@gmail.com)
 */
public class BaseRoom implements Room {
	private String name;
	private Message description;
	private Collection<GameObject> objectsInRoom = new ArrayList<>();
	private ArrayList<Path> paths = new ArrayList<>();
        private Message briefDescription;
        private int state;
        private HashMap<String, String> properties = new HashMap<>();
	
	public BaseRoom (String name, Message desc, Message brief){
		this.name = name;
		this.description = desc;
                briefDescription = brief;
				
	}
	@Override
	public List<Path> getPaths(){
		return paths;
	}
	@Override
	public void addPath(Path path){
		paths.add(path);
	}
	
        @Override
        public void addObject(GameObject obj){
		objectsInRoom.add(obj);
	}
	
   
        @Override
	public void removeObject(GameObject obj){
		objectsInRoom.remove(obj);
	}
	
    
        @Override
	public boolean contains(GameObject obj){
           // System.out.println(obj.getInventoryDesc());
            return objectsInRoom.contains(obj);
        }
	
        @Override
        public Message getBriefDescription(){
            return briefDescription;
        }
        
        @Override
	public Collection <GameObject> getContents(){
		return objectsInRoom;
	}

	@Override
	public Message getDescription(){
            return description;
	}
	@Override
        public int getState(){
            return state;
        }
        @Override
        public void setState(int i){
            state = i;
        }
        @Override
        public String getProp(String propName){
            if(properties.containsKey(propName)){
                return properties.get(propName);
            }
            return "";
        }
        
        @Override
        public void setProp(String propName, String value){
            properties.put(propName, value);

        }

}