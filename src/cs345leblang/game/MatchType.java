

package cs345leblang.game;

/**
 * This enumerations specifies the type of word match for Words and
 * the command parser.
 * 
 * @author Gaelen LeBlanc (gaelen.leblanc@gmail.com)
 */
public enum MatchType {
    
    PREFIX, EXACT
}

