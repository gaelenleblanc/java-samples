
package cs345leblang.game;

import java.util.*;
import cs345leblang.interpret.*;

/**
 * @author Gaelen LeBlanc (gaelen.leblanc@gmail.com)
 */
 
 public class GameObjects implements GameObject{
        private String label;
        private Term identifier;
        private Message invDesc;
        private Message hereIsDes;
        private Message longD;
        private ArrayList<GameObject> inventory = new ArrayList<>();
        private int state;
        private HashMap<String, String> properties = new HashMap<>();

        public GameObjects(String name, Term vocab, Message inventoryDesc, Message hereIsDesc, Message longDesc){
                label = name;
                identifier = vocab;
                invDesc = new BoundSelectMessage(inventoryDesc, this);
                hereIsDes = new BoundSelectMessage(hereIsDesc, this);
                longD = new BoundSelectMessage (longDesc, this);
                state = 0;
        }

        public String getName(){
                return this.label;
        }
        @Override
        public void addObject(GameObject obj){
            inventory.add(obj);
        }

        @Override 
        public void removeObject(GameObject obj){
           inventory.remove(obj);
        }


        @Override
        public boolean contains(GameObject obj){
            return inventory.contains(obj);
        }

        @Override
        public Collection<GameObject> getContents(){
            return inventory;
        }

        public Term getIdentifyingTerms(){
                return this.identifier;
        }
        @Override
        public Message getInventoryDesc(){
                return this.invDesc;
        }
        @Override
        public Message getHereIsDesc(){
                return this.hereIsDes;
        }
        @Override
        public Message getLongDesc(){
                return this.longD;
        }
        @Override
        public boolean match(Word w){
            return identifier.termContainsWord(w);
        }
        @Override
        public int getState(){
            return state;
        }
        @Override
        public void setState(int i){
            state = i;            
        }
        @Override
        public String getProp(String propName){
            if(properties.containsKey(propName)){
                return properties.get(propName);
            }
            return "";
        }
        
        @Override
        public void setProp(String propName, String value){
            properties.put(propName, value);
        }
 
 }