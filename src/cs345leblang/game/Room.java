

package cs345leblang.game;
import java.util.*;
import cs345leblang.interpret.*;

/**
 * This is the interface provided for Rooms.
 * 
 * @author Gaelen LeBlanc (gaelen.leblanc@gmail.com)
 */
public interface Room extends Container {
	
        public Message getBriefDescription();
    
	public Message getDescription();
        
        public Collection<Path> getPaths();
        
        public void addPath(Path path);
}
