
package cs345leblang.game;
import cs345leblang.interpret.*;

/**
 * This is the interface provided for Game Objects.
 * 
 * @author Gaelen LeBlanc (gaelen.leblanc@gmail.com)
 */
public interface GameObject extends Container{

    
    public boolean match(Word w);
	
    public Message getInventoryDesc();
    
    public Message getHereIsDesc();
    
    public Message getLongDesc();
}
