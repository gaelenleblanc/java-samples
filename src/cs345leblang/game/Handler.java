
package cs345leblang.game;

import cs345leblang.interpret.*;
/**
 *
 * @author Gaelen LeBlanc (gaelen.leblanc@gmail.com)
 */
public class Handler {
    private Event e;
    private HandlerMethod handler;
    
    public Handler(Event event, HandlerMethod hanMeth){
        e = event;
        handler = hanMeth;
    }
    
    public HandlerMethod getHandlerMethod(){
        return handler;
    }
    
    public Event getEvent(){
        return e;
    }
    
    
}
