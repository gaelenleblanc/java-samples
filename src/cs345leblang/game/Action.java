

package cs345leblang.game;

/**@author Gaelen LeBlanc (gaelen.leblanc@gmail.com)
 */
 
 public class Action {
	private Term term1;
	private Term term2; 
	private ActionMethod method;
        private int priority;
        private ValidMethod vMeth;
	
	public Action(Term term1, Term term2, int i, ValidMethod vMethod, ActionMethod method){
		this.term1 = term1;
		this.term2 = term2;
		this.method = method;
                priority = i;
                this.vMeth= vMethod;
	}
	
        public int getPriority(){
            return priority;
        }
        
        
        public ValidMethod getValidMethod(){
            return vMeth;
        }
        
	public Term getTermOne(){
		return this.term1;
	}
        
	public Term getTermTwo(){
		return this.term2;
	}
        
	public ActionMethod getMethod(){
		return this.method;
	}
        
	
 
 }