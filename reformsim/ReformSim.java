package reformsim;
/**
 *
 * @author Gaelen LeBlanc
 * this program was written and designed for WWU math 410 course. 
 */
import java.io.*;
import java.math.*; 
import java.util.*;
import java.lang.*;

        
public class ReformSim {
    
    public static void main(String[] args) {
        //change with initial parameters
        int cycles = 500; //how many cycles wish to observe
        ArrayList<Person> population = new ArrayList<>();
        ArrayList<Person> criminalPop = new ArrayList<>();
        ArrayList<Person> jailPop = new ArrayList<>();
        for(int i = 0; i < 100000 ; i++){
            Person toAdd = new Person(0);
            population.add(toAdd);
            }
        double crimeProb = 0;
        
        while( cycles >0 ){
            //change with initial parameters
            double a = .00010*population.size(); // (10%) probability of an individual committing a crime that would warrant jail time
        
        
            for(int j = population.size() - 1; j> 0; j--){
                crimeProb += population.get(j).getCrimeProb();
            
                if(crimeProb >= a){
                    criminalPop.add(population.get(j));
                    population.remove(j);
                    crimeProb = 0;
                }
            }
            //change with initial parameters
            double b = .75*criminalPop.size(); //probability of being caught and sent to jail (those caught are sent to jail)
        
            int counter = 0;
            int arrests = 0;
            for(int i = criminalPop.size() -1; i > 0; i--){
                counter++;
            
                if(counter <= b){
                   criminalPop.get(i).addToJail(1);
                    jailPop.add(criminalPop.get(i));
                    criminalPop.remove(i);
                    arrests++;
                }
            }
            //change with initial parameters
            double k = .15*jailPop.size(); //probability of reformation and release from prison back into population.
            counter = 0; //reuse counter
            for(int i = jailPop.size() - 1 ; i > 0; i--){
                counter++;
                if(counter <= k){
                    jailPop.get(i).setCrimeProb(.9); //reformed inmates have ten percent less of a chance to commit crimes
                    population.add(jailPop.get(i));
                    jailPop.remove(i);//inmate has been removed from jail and placed back into population
                }   
            }
            //post reformation program
            Collections.shuffle(population);
            Collections.shuffle(jailPop);
            Collections.shuffle(criminalPop);
            System.out.println(500 - cycles);
            System.out.println("Ratio of population to criminals (not in jail): ");
            double ratioPC = (population.size()+criminalPop.size()) / criminalPop.size();
            System.out.println(ratioPC + " : 1");
            System.out.println("Number of arrests made: " + arrests);
            System.out.println("Population including criminals: " + population.size());
            System.out.println("Jail population after reformation: " + jailPop.size());
            System.out.println("Criminals avoiding arrest: " + criminalPop.size()+ "\n\n");
            cycles--;
        }
        
    }
}
