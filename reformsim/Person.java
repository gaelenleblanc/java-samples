package reformsim;


import java.util.*;
import java.math.*;
import java.io.*;
import java.lang.*;
/**
 *
 * @author Gaelen LeBlanc
 */
public class Person {
    private int timesInJail;
    private int inJail;
    private double probOfCommittingCrime;
    private double probOfJail;
    
    
    public Person(int i){
        timesInJail = i;
        probOfCommittingCrime = 1;
        inJail = 0;
        probOfJail = 1;
    }
    
    public int getTimesInJail(){
        return timesInJail;
    }
    
    public void addToJail(int i){
        timesInJail += i;
    }
    public int inJail(){
        return inJail;
    }
    public void setJail(int i){
        inJail = i;
    }
    public double getCrimeProb(){
        return probOfCommittingCrime;
    }
    public void setCrimeProb(double a){
        probOfCommittingCrime *= a;
    }
    public double getProbOfJail(){
        return probOfJail;
    }
    
}
