package demo.shopping;

//@author Gaelen LeBlanc
import java.io.*;
import java.sql.*;
import java.util.Scanner;
import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


//@WebServlet("/WelcomingUser")
public class Shopp extends HttpServlet {
	//private static final long serialVersionUID = 1L;
	public Connection conn = null ;
	String blueMixURL = "";
	String dbName = "";
	String usName = "";
	String pswd = "";
	String jdbcurl1;
	
	public void getConnection(){
		
		try{
			conn = DriverManager.getConnection(jdbcurl1, usName, pswd);
			System.out.println("Successfully established connection.");
		}
		catch(SQLException e){
			System.out.println("Unable to establish Connection: " );
			e.printStackTrace();
		}
				
	}
	
	
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String Username = request.getParameter("username");
		String password = request.getParameter("password");
		
		PrintWriter out = response.getWriter();
		response.setContentType("text/html");
		jdbcurl1 = "jdbc:mysql:" + blueMixURL + dbName;
		getConnection();
		try{
		
			if(VerifyUser(conn, Username, password)){
				
			
					HttpSession session = request.getSession(true);
					session.setAttribute("username", Username);
					request.getRequestDispatcher("Table.jsp").forward(request, response);
					
			}
			
			else{
					out.println("Invalid username and password.");
					
			}
				
				
				
//				out.println(Username + "! Welcome to HappyVisit Online Shopping.<BR>");
//				out.println("<HTML><BODY>");
//				out.println("<HR>");
//				out.println("<FORM ACTION = http://localhost:9080/ShoppingDemo/ChoiceProcessingServlet METHOD=POST>");
//				out.println("<TABLE WIDTH = 500>");
//				out.println("<TR><TH>ITEM NO </TH> <TH> Brands of Trousers </TH> <TH>BUY</TH></TR>");
//				out.println("<TR><TD>1 </TD> <TD> PeterEngland </TD> <TD><INPUT Name = c1 TYPE = CHECKBOX VALUE = PeterEngland></TD></TR>");
//				out.println("<TR><TD>2 </TD> <TD> Moustache </TD> <TD><INPUT Name = c2 TYPE = CHECKBOX VALUE = Moustache></TD></TR>");
//				out.println("<TR><TD>3 </TD> <TD> AllenSolly </TD> <TD><INPUT Name = c3 TYPE = CHECKBOX VALUE = AllenSolly></TD></TR>");
//				out.println("<TR><TD>4 </TD> <TD> Zodiac </TD> <TD><INPUT Name = c4 TYPE = CHECKBOX VALUE = Zodiac></TD></TR>");
//				out.println("</TABLE>");
//				out.println("<INPUT TYPE = SUBMIT VALUE = SUBMIT>");
//				out.println("</FORM>");
//				out.println("</BODY></HTML>");
//				out.close();
				
				}
			
		catch(SQLException e){
			e.printStackTrace();
		}
		
		try {
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		

	}
	
	public void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException{
		doGet(request, response);
	}
	

	
	public static boolean VerifyUser(Connection con,String username,  String password) throws SQLException{
		
		PreparedStatement stmt = null;
		String query = "SELECT PASSWORD FROM USERS WHERE USERNAME = ?" ;
		
		
		
		String toquery = username;
		String pword = "";
		//System.out.println(toquery);
		
		try{
			
			stmt = con.prepareStatement(query);
			
			stmt.setString(1, toquery);
			
			
			
			
			ResultSet rs = stmt.executeQuery();
			
			
			while (rs.next()){
				pword = rs.getString("PASSWORD");
			}
			
			
		}
		catch(SQLException e){
			
			System.out.println("Invalid Login.");
		}
		
		return pword.equals(password);
		
	}

	
}